#!/bin/bash
REGISTRY=registry.prf.gov.br/fabio.barros/rota191
IMAGE=rota191-backend
VERSAO=1.0.1
#$(grep app_version gradle.properties | cut -d '=' -f2)
docker build . -t $REGISTRY/$IMAGE:$VERSAO
docker push $REGISTRY/$IMAGE:$VERSAO
echo "Versao nova da imagem $IMAGE  no registry!!"
