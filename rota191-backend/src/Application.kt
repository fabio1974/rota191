package br.gov.prf.erdsi

import br.gov.dprf.wsclient.ConfigWeb
import br.gov.dprf.wsclient.pd.WSParteDiaria
import br.gov.dprf.wsclient.pd.dominio.ParteDiaria
import br.gov.dprf.wsclient.pd.dominio.Policial
import br.gov.dprf.wsclient.pd.dominio.TipoParteDiaria
import br.gov.prf.ecomultas.mpir.security.installRouteLogin
import br.gov.prf.ecomultas.mpir.security.simpleJwt
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import io.ebean.Ebean
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.jwt.jwt
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.util.InternalAPI
import io.ktor.util.toLocalDateTime
import persistence.Pessoa
import persistence.PessoaMapper
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


fun main(args: Array<String>): Unit = io.ktor.server.cio.EngineMain.main(args)

@UseExperimental(InternalAPI::class)
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    println("Rota191 - Versão 1.0.1")

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.AccessControlAllowOrigin)
        allowCredentials = true
        anyHost()
    }


    installRouteLogin(this)


    install(Authentication) {
        jwt {
            verifier(simpleJwt.verifier)
            validate {
                UserIdPrincipal(it.payload.getClaim("cpf").asString())
            }
        }
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            registerModule(JavaTimeModule())
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        }
    }

    routing {
        route("api") {
            get("/findAllByDates") {
                /*
                        val  uf = call.parameters["uf"]
                        val  dataInicio = call.parameters["dataInicio"]
                        val  dataFim = call.parameters["dataFim"]
                        System.out.print("uf :" +uf +"\n")
                        System.out.print("data Inicio :" +dataInicio + "\n")
                        System.out.print("data fim :" + dataFim)
                        call.respond(HttpStatusCode.OK,uf + "-" + dataInicio + "-" + dataFim)
            */

                //val obj = call.receiveOrNull<Filtro>()

                val sql =
                    " select DISTINCT pes.cpf as cpf,pes.nome as nome,pes.matricula as matricula,pes.email as email, " +
                            " pes.lotacao as lotacao ,equ.regional_descricao as regional_descricao, equ.imei_interno as imei_interno, " +
                            " loc.loc_data as loc_data,loc.loc_latitude as loc_latitude, loc.loc_longitude as loc_longitude,loc.loc_velocidade as loc_velocidade" +
                            " from pessoa pes  inner join equipamento equ on pes.cpf = equ.cpf_responsavel" +
                            " inner join localizacao loc on loc.loc_imei = equ.imei_interno" +
                            " inner join (select loc_imei, max(loc1.loc_data) ultloc from localizacao loc1 " +
                            " where loc1.loc_data >= DATE_ADD(NOW(), INTERVAL -3 HOUR) and loc1.loc_data <= NOW() and loc1.loc_imei is not null  group by 1)  loc2 on (loc.loc_imei = loc2.loc_imei and loc.loc_data = loc2.ultloc)" +
                            " order by loc_data desc"
                val ret = Ebean.createSqlQuery(sql).findList(PessoaMapper())
                call.respond(HttpStatusCode.OK, ret)
            }

            get("/findTeste/{uf?}/{dataInicio?}/{dataFim?}") {

                val uf = call.parameters["uf"]
                val dataInicio = call.parameters["dataInicio"]
                val dataFim = call.parameters["dataFim"]
                val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.getDefault())
                val formatterStringDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val resultInicio = formatter.parse(dataInicio)
                val resultFim = formatter.parse(dataFim)
                val inicio = formatterStringDate.format(resultInicio)
                val fim = formatterStringDate.format(resultFim)

                //System.out.print("uf :" + uf)
                System.out.print("data Inicio :" + dataInicio)
                System.out.print("data fim :" + dataFim)
                ConfigWeb.setModo(ConfigWeb.MODO_PRODUCAO)

                var pdiTodasPorUf: MutableList<ParteDiaria>? = null
                //@Todo Ajustas TimeZone??
                var timezone = TimeZone.getDefault().id
                //var timezone = "America/Sao_Paulo"
                val localDateTime = LocalDateTime.now(ZoneId.of(timezone))
                val date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())
                var policiaisPdiOprPorUf = ArrayList<Policial>()
                var policiaisCpf = ArrayList<String>()
                var countCpfs: Int = 0;
                var cpfs = ""

                try {
                    pdiTodasPorUf = WSParteDiaria.getInstance().pesquisaPartesDiariasPorUfEDataHora(uf, date)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                pdiTodasPorUf?.forEach { pdi ->
                    if (pdi.tipo.descricao.equals(TipoParteDiaria.OPERACIONAL.descricao)) {
                        pdi.policiais?.forEach { policial ->
                            policiaisPdiOprPorUf.add(
                                policial
                            )
                            policiaisCpf.add(policial.cpf)
                            if (countCpfs > 0)
                                cpfs =
                                    cpfs + ",\"" + policiaisCpf.get(countCpfs).removePrefix("[").removeSuffix("]") + "\""
                            else
                                cpfs =
                                    cpfs + "\"" + policiaisCpf.get(countCpfs).removePrefix("[").removeSuffix("]") + "\""
                            countCpfs++
                        }
                    }
                }
                val sql =
                    " select DISTINCT pes.cpf as cpf,pes.nome as nome,pes.matricula as matricula,pes.email as email, " +
                            " pes.lotacao as lotacao ,equ.regional_descricao as regional_descricao, equ.imei_interno as imei_interno, " +
                            " loc.loc_data as loc_data,loc.loc_latitude as loc_latitude, loc.loc_longitude as loc_longitude,loc.loc_velocidade as loc_velocidade" +
                            " from pessoa pes  inner join equipamento equ on pes.cpf = equ.cpf_responsavel and pes.cpf in  ($cpfs) inner join localizacao loc on loc.loc_imei = equ.imei_interno inner join (select loc_imei, max(loc1.loc_data) " +
                            "ultloc from localizacao loc1  where loc1.loc_data >= \"$inicio\" and loc1.loc_data <= \"$fim\" and loc1.loc_imei is not null  group by 1)  loc2 on (loc.loc_imei = loc2.loc_imei and loc.loc_data = loc2.ultloc)" +
                            " order by loc_data desc"

                val ret = Ebean.createSqlQuery(sql).findList(PessoaMapper())
                val pessoa: MutableList<Pessoa>? = ret
                call.respond(HttpStatusCode.OK, ret)

                //val ret = DB.byName("mysql").createSqlQuery(sql).findList(PessoaMapper())
                //call.respond(HttpStatusCode.OK,ret)
                // val sql2 = "select first 1 * from inventario_eda:informix.equipamento where cpfResponsavel = \"61694282368\" and status = 1 order by data_cadastramento desc"

                // val ret2 = DB.byName("ifxprod").createSqlQuery(sql2).findList(EquipamentoInventarioMapper())
                // call.respond(HttpStatusCode.OK,ret2)
            }
        }
    }


}

class Filtro(
    var dataInicio: ZonedDateTime? = null,
    val dataFim: ZonedDateTime? = null,
    var uf: String? = null
)


fun getNextValSequence(sequence: String? = null): Long? {
    val ret = Ebean.createSqlQuery("SELECT ${sequence}.nextval as seq FROM systables WHERE tabid = 1").findSingleLong()
    return ret;
}

