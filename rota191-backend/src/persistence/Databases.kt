package persistence

import io.ebean.DB

val PG = DB.byName("pg")
val IFX = DB.byName("ifx")
val IFXPROD = DB.byName("ifxprod")
