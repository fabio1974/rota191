package br.gov.prf.erdsi.persistence

import java.time.LocalDateTime

data class EquipamentoInventario (
    val id:Int?=null,
    val serialInterno:String?=null,
    val regional:String?=null,
    val situacao:Int?=null,
    val status:Int?= null,
    val dataCadastramento:LocalDateTime?=null,
    val dataUltimaAtualizacao:LocalDateTime?=null,
    val telefone:String?=null,
    val imsiInterno:String?=null,
    val operadora:String?=null,
    val cpfResponsavel:String?=null,
    val funcional:Boolean?=null,
    val descricao:String?=null

)


