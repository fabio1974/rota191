package br.gov.prf.erdsi.persistence


import io.ebean.RowMapper
import io.ktor.util.InternalAPI
import io.ktor.util.toZonedDateTime
import java.sql.ResultSet
import java.sql.SQLException

class EquipamentoInventarioMapper : RowMapper<EquipamentoInventario> {

    @UseExperimental(InternalAPI::class)
    @Throws(SQLException::class)
    override fun map(rset: ResultSet, rowNum: Int): EquipamentoInventario {

        return EquipamentoInventario(
            id = rset.getInt("id_equipamento"),
            serialInterno =  rset.getString("serial_interno"),
            regional =  rset.getString("fk_id_regional"),
            situacao =  rset.getInt("fk_id_situacao"),
            status =  rset.getInt("status"),
            dataCadastramento =  rset.getTimestamp("data_cadastramento").toLocalDateTime(),
            dataUltimaAtualizacao = rset.getTimestamp("data_ultima_atualizacao").toLocalDateTime(),
            telefone = rset.getString("numero_telefone_chip"),
            imsiInterno = rset.getString("numero_codigo_chip"),
            operadora = rset.getString("nome_operadora"),
            cpfResponsavel = rset.getString("cpfresponsavel"),
            funcional = rset.getBoolean("isfuncional"),
            descricao = rset.getString("descricao")
        )
    }
}
