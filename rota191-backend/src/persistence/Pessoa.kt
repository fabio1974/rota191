package persistence

import java.time.ZonedDateTime

data class Pessoa(
    val cpf:String?=null,
    val nome:String?= null,
    val matricula:String?=null,
    val email: String?=null,
    val lotacao:String?=null,
    val regionalDescricao: String?=null,
    val imeiInterno: String?=null,
    val locData: ZonedDateTime?=null,
    val locLatitude: Double?=null,
    val locLongitude: Double?=null,
    val locVelocidade: Double?=null
)
