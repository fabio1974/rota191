package persistence


import io.ebean.RowMapper
import io.ktor.util.InternalAPI
import io.ktor.util.toZonedDateTime
import java.sql.ResultSet
import java.sql.SQLException

class PessoaMapper : RowMapper<Pessoa> {

    @UseExperimental(InternalAPI::class)
    @Throws(SQLException::class)
    override fun map(rset: ResultSet, rowNum: Int): Pessoa {

        return Pessoa(
            cpf = rset.getString("cpf"),
            nome =  rset.getString("nome"),
            matricula =  rset.getString("matricula"),
            email =  rset.getString("email"),
            lotacao =  rset.getString("lotacao"),
            regionalDescricao =  rset.getString("regional_descricao"),
            imeiInterno = rset.getString("imei_interno"),
            locData = rset.getTimestamp("loc_data").toZonedDateTime(),
            locLongitude = rset.getDouble("loc_latitude"),
            locLatitude = rset.getDouble("loc_longitude"),
            locVelocidade = rset.getDouble("loc_velocidade")
        )
    }
}
