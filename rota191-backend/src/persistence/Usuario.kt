package persistence

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

data class Funcionalidades(var siglasDasFuncionalidades: Array<String>? = null)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Usuario(
    val id: Int?=null,
    val cpf: String? = null,
    val nome: String? = null,
    val codigoSiscom: String? = null,
    val uf: String? = null,
//    val senha: String? = null,
    val matricula: String? = null,
    val lotacao: String? = null,
    val ufLotacao: String? = null,
//    val telefoneContato: String? = null,
//    val situacao: Int? = null,
//    val descricaoTipo: String? = null,
    val nomeGuerra: String? = null,
//    val area: Int? = null,
//    val funcao: Any? = null,
//    val cargo: Int? = null,
//    val dataAtivacao: String? = null,
//    val dataDesativacao: String? = null,
//    val excluido: Boolean? = null,
//    val somenteNovaSenha: Boolean? = null,
//    val usuarioExterno: Boolean? = null,
//    val classeGeradora: String? = null,
//    val usuarioLogin: Any? = null,
    //val roles: Array<String>? = null,
    val controlePermissoes: Funcionalidades? = null
//    val permissoes: List<Funcionalidade>
)
