package br.gov.prf.ecomultas.mpir.security





import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import persistence.Usuario
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.InetAddress
import java.net.URL

fun installRouteLogin(application: Application) {


    application.routing {
      route("api") {
          //Login de Teste
          post("loginhomo") {
              val post = call.receive<LoginPayload>()
              val usuario = users.get(post.cpf)
              if (usuario != null) {
                  call.respond(mapOf("token" to simpleJwt.sign(usuario)))
              } else
                  call.respond(HttpStatusCode.NotFound)
          }


          //Login de Produção no DPrfseguranca
          post("/login") {
              val post = call.receive<LoginPayload>()
              try {
                  val ip = InetAddress.getLocalHost()
                  var urlStr =
                      "http://dprfseg.prf/dprfseguranca/ws/login?cpf=${post.cpf}&senha=${post.senha}&ip=${ip.hostAddress}&siglaSistema=SISCOM&cript=${false}"
                  val conn = URL(urlStr).openConnection() as HttpURLConnection
                  conn.setRequestMethod("GET")
                  conn.setRequestProperty("Accept", "application/json")
                  // conn.setConnectTimeout(5000); //5 segundos de timeout
                  if (conn.getResponseCode() == 200) {
                      val br = BufferedReader(InputStreamReader(conn.getInputStream()))
                      val response = br.readLine()
                      val usuario: Usuario = jacksonObjectMapper().readValue<Usuario>(
                          response,
                          Usuario::class.java
                      )
                      println("Usuário $usuario")
                      conn.disconnect()
                      call.respond(mapOf("token" to simpleJwt.sign(usuario)))
                  } else if (conn.getResponseCode() == 204) {
                      conn.disconnect()
                      call.respond(HttpStatusCode.NotFound)
                  } else {
                      conn.disconnect()
                      throw Exception("FalhaSeguranca")
                  }
              } catch (e: Exception) {
                  throw e
              }
          }
      }
    }
}
