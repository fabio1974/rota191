package br.gov.prf.ecomultas.mpir.security



import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.jwt.jwt
import io.ktor.features.CORS
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import persistence.Funcionalidades
import persistence.Usuario

import java.util.*




open class SimpleJWT(val secret: String) {
    private val algorithm = Algorithm.HMAC256(secret)
    val verifier = JWT.require(algorithm).build()
    fun sign(usuario: Usuario): String {
        var expireDate = Calendar.getInstance()
        expireDate.add(Calendar.HOUR,2)
        return JWT.create()
            .withArrayClaim("roles", usuario.controlePermissoes!!.siglasDasFuncionalidades)
            .withIssuedAt(Date())
            .withExpiresAt(expireDate.time)
            .withClaim("cpf", usuario.cpf)
            .withClaim("nome", usuario.nome)
            .withClaim("codigoSiscom", usuario.codigoSiscom)
            .withClaim("lotacao", usuario.lotacao)
            .withClaim("ufLotacao", usuario.uf)
            .withClaim("id", usuario.id)
            .withClaim("matricula", usuario.matricula)
            .sign(algorithm)
    }
}

class LoginPayload(val cpf: String, val senha: String)

val simpleJwt = SimpleJWT("my-super-secret-for-jwt")
val users = Collections.synchronizedMap(
    listOf(
        Usuario(
            708,
            "28272738880",
            "Fabio Barros",
            matricula="1480469",
            //"1234",
            uf="MT",
            lotacao="1600",
            controlePermissoes = Funcionalidades(arrayOf("role1", "role2", "role3"))
        ),
        Usuario(
            12345,
            "22222222222",
            "Carlos Ferreira",
            matricula="1234567",
            //"1234",
            uf="MT",
            lotacao="1400",
            controlePermissoes = Funcionalidades(arrayOf("role3", "role4", "role5"))
        ),
        Usuario(
            54321,
            "11111111111",
            "Alinson Donato",
            matricula="7654321",
            //"1234",
            uf="MT",
            lotacao="1600",
            controlePermissoes = Funcionalidades(arrayOf("role6", "role7", "role8"))
        )
        )
        .associateBy {it.cpf }
        .toMutableMap()
)
//------------------------------------------------------------




fun installSecurity(application: Application){

    application.install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        header("MyCustomHeader")
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }


    application.install(Authentication) {
        jwt {
            verifier(simpleJwt.verifier)
            validate {
                UserIdPrincipal(it.payload.getClaim("cpf").asString())
            }
        }
    }


}




