#!/bin/bash
REGISTRY=registry.prf.gov.br/fabio.barros/rota191
IMAGE=rota191-frontend
VERSAO=1.0.1
#$(grep VERSAO src/environments/versao.ts | cut -d '"' -f2)
echo "docker build . -t $REGISTRY/$IMAGE:$VERSAO"
docker build . -t $REGISTRY/$IMAGE:$VERSAO
echo "docker push $REGISTRY/$IMAGE:$VERSAO"
docker push $REGISTRY/$IMAGE:$VERSAO
echo "Versao nova da imagem $IMAGE no registry!!"


