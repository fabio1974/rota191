// server.js
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()

server.use(middlewares)

// login
const jwt = require("jsonwebtoken");
const APP_SECRET = "myappsecret";
const USERNAME = "admin";
const PASSWORD = "secret";
const NAME = "Marcone de Souza Santana";
const CPF = "03119541621";

// Add custom routes before JSON Server router
server.get('/api/login', (req, res) => {
    res.jsonp(req.query)
})

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)

server.use((req, res, next) => {
    if (req.url == "/api/login" && req.method == "POST") {
        if (req.body != null && req.body.usuario === USERNAME
            && req.body.senha === PASSWORD) {
                console.log(req.body);
            let token = jwt.sign({ data: USERNAME, name: NAME, cpf: CPF }, APP_SECRET, { expiresIn: "1h" });
            res.json({ success: true, token: token });
        } else {
            res.sendStatus(401)
        }
        res.end();
        return;
    }
    else {
        let token = req.headers["authorization"];
        if (token != null && token.startsWith("Bearer ")) {
            token = token.substring(7, token.length);
            try {
                jwt.verify(token, APP_SECRET);
                next();
                return;
            } catch (err) {
                res.statusCode = 401;
            }
        } else {
            res.statusCode = 401;
            res.end();
            return;
        }

    }
    next();
})
// server.use(router)
server.use('/api', router)
server.listen(3000, () => {
    console.log('JSON Server is running')
})