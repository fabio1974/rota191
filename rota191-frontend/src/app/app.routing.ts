import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './core/layout/layout.component';

import { DashboardComponent } from './core/dashboard/dashboard.component';
//import { AuthGuard } from './core/_guards/auth.guard';
import { FaleConoscoComponent } from './core/fale-conosco/fale-conosco.component';
import {MapaPrfComponent} from './mapas/mapa-prf/mapa-prf.component';
import {AuthGuard} from './seguranca/auth.guard';
import {LoginComponent} from './seguranca/login/login.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'login',
        component: LoginComponent,
        data: {
            title: 'Login',
            login: 'true'
        }
    },
    {
        path: '',
        component: LayoutComponent,
        children: [

            /*{
                path: 'dashboard',
                component: DashboardComponent,
                canActivate: [AuthGuard],
                data: {
                    title: 'Dashboard'
                }
            },*/

          {
            path: 'mapaPrf',
            component: MapaPrfComponent,
            canActivate: [AuthGuard],
            data: {
              title: 'Mapa'
            }
          },
            {
                path: 'fale-conosco',
                component: FaleConoscoComponent,
                canActivate: [AuthGuard],
                data: {
                    title: 'Fale Conosco'
                }
            },
            {
                path: 'func',
                loadChildren: () => import('./funcionalidade/funcionalidade.module').then(m => m.FuncionalidadeModule),
                data: {
                    title: 'Funcionalidade'
                }
            }
        ]
    }
];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouting { }
