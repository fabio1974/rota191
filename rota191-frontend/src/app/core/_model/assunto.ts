export class Assunto {
    public static RECLAMACAO = new Assunto('Reclamação');
    public static SUGESTAO = new Assunto('Sugestão');
    public static ELOGIO = new Assunto('Elogio');
    public static DUVIDA = new Assunto('Dúvida');
    public static SOLICITACAO = new Assunto('Solicitação');
    public static OUTROS = new Assunto('Outros');

    private constructor(
        private descricao: string
    ) { }

    public static getTodos(): Assunto[] {
        return [Assunto.RECLAMACAO, Assunto.SUGESTAO, Assunto.ELOGIO,
        Assunto.DUVIDA, Assunto.SOLICITACAO, Assunto.OUTROS];
    }
}
