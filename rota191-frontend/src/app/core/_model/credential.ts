export class Credential {
    public usuario: string;
    public senha: string;
    public token: string;
    public captcha: string;
}

export class User {
    constructor(
        public token: string,
        public nome: string,
        public permissoes: string[]
    ) { }
}
