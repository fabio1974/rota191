export class Mensagem {
    assunto: string;
    mensagem: string;
    sistemaId: number;
    usuarioCpf: string;
}
