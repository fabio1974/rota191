interface NavAttributes {
    [propName: string]: any;
}
interface NavWrapper {
    attributes: NavAttributes;
    element: string;
}
interface NavBadge {
    text: string;
    variant: string;
}
interface NavLabel {
    class?: string;
    variant: string;
}

export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: NavBadge;
    title?: boolean;
    children?: NavData[];
    variant?: string;
    attributes?: NavAttributes;
    divider?: boolean;
    class?: string;
    label?: NavLabel;
    wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
    /*{
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-speedometer',
    },*/
  {
    name: 'Mapa PRFs',
    url: '/mapaPrf',
    icon: 'fa fa-map-o'
  },
    {
        name: 'Funcionalidade',
        url: '/func',
        icon: 'icon-note',
        children: [
            {
                name: 'Exemplo 1',
                url: '/func/exemplo1',
                icon: 'icon-pencil'
            },
            {
                name: 'Exemplo 2',
                url: '/func/exemplo2',
                icon: 'icon-pencil'
            }
        ]
    },
    {
        name: 'Fale Conosco',
        url: '/fale-conosco',
        icon: 'icon-support',
        class: 'mt-auto',
        variant: 'success'
    }
];
