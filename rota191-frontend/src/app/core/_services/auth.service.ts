import { Injectable } from '@angular/core';
import { Credential, User } from '../_model/credential';
import { HttpClient } from '@angular/common/http';
import { tap, shareReplay } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

const session = 'jwt_sessao';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jwtHelper = new JwtHelperService();

  constructor(
    private http: HttpClient
  ) { }

  login(credential: Credential) {
    return this.http.post('/api/login', credential, { observe: 'response' }).pipe(
      tap((response: any) => {
        this.createSession(response);
      }),
      shareReplay()
    );
  }

  logout(): void {
    this.removeSession();
  }

  createSession(response: any): void {
    if (response) {
      const token = response.body.token;
      const payload = this.jwtHelper.decodeToken(token);
      localStorage.setItem(session, JSON.stringify(new User(token, payload.name, undefined)));
    }
  }

  removeSession(): void {
    localStorage.removeItem(session);
  }

  get token(): string {
    return localStorage.getItem(session) ? JSON.parse(localStorage.getItem(session)).token : undefined;
  }
  get autenticado(): boolean {
    return this.token && !this.jwtHelper.isTokenExpired(this.token);
  }

  get nomeUsuario(): string {
    return this.token ? this.jwtHelper.decodeToken(this.token).name : undefined;
  }

  get cpfUsuario(): string {
    return this.token ? this.jwtHelper.decodeToken(this.token).cpf : undefined;
  }

  salvarFalhaLogin(): void {
    localStorage.setItem('falha_login', 'true');
  }

  salvarLogout(): void {
    localStorage.setItem('logout', 'true');
  }

  houveFalhaLogin(): boolean {
    return localStorage.getItem('falha_login') ? true : false;
  }

  logoutRealizado(): boolean {
    return localStorage.getItem('logout') ? true : false;
  }

  cancelaFalhaLogin(): void {
    localStorage.removeItem('falha_login');
  }

  cancelaLogout(): void {
    localStorage.removeItem('logout');
  }
}
