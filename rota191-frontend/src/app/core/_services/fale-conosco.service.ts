import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Mensagem } from '../_model/mensagem';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class FaleConoscoService {

  sistemaId = 19999; // id do sistema no PRFSegurança

  constructor(
    private auth: AuthService,
    private http: HttpClient
  ) { }

  salvarMensagem(mensagem: Mensagem): Observable<Mensagem> {
    mensagem.usuarioCpf = this.auth.cpfUsuario;
    mensagem.sistemaId = this.sistemaId;
    return this.http.post<Mensagem>('/api/mensagens', mensagem);
  }
}
