import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AppAsideModule, AppBreadcrumbModule, AppHeaderModule, AppFooterModule, AppSidebarModule } from '@coreui/angular';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ToastrModule } from 'ngx-toastr';
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


import { LayoutComponent } from './layout/layout.component';
import { MenuSistemasComponent } from './menu-sistemas/menu-sistemas.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { FaleConoscoComponent } from './fale-conosco/fale-conosco.component';
import { AvisosComponent } from './avisos/avisos.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { SharedModule } from '../shared/shared.module';
import {LoginComponent} from '../seguranca/login/login.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    BsDropdownModule,
    CarouselModule,
    AlertModule,
    ModalModule,
    PerfectScrollbarModule,
    SharedModule,
    TabsModule,
    ToastrModule,
    DialogModule,
    ProgressSpinnerModule
  ],
  declarations: [
    LayoutComponent,
    MenuSistemasComponent,
    DashboardComponent,
    FaleConoscoComponent,
    AvisosComponent
  ],
  exports: [
    LayoutComponent,
    MenuSistemasComponent,
    DashboardComponent,
    FaleConoscoComponent,
    AvisosComponent
  ]
})
export class CoreModule { }
