import { Component, OnInit, ErrorHandler } from '@angular/core';
import { Assunto } from '../_model/assunto';
import { FaleConoscoService } from '../_services/fale-conosco.service';
import { Mensagem } from '../_model/mensagem';
import { NgForm, FormControl, FormControlDirective, FormControlName } from '@angular/forms';
import { AlertService } from 'src/app/core/_services/alert.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-fale-conosco',
  templateUrl: './fale-conosco.component.html',
  styleUrls: ['./fale-conosco.component.css']
})
export class FaleConoscoComponent implements OnInit {

  assuntos: Assunto[];

  mensagem: Mensagem;

  constructor(
    private alertService: AlertService,
    private faleConoscoService: FaleConoscoService
  ) { }

  ngOnInit() {
    this.assuntos = Assunto.getTodos();
    this.mensagem = new Mensagem();
  }

  enviar(form: NgForm): void {
    if (form.valid) {
      this.faleConoscoService.salvarMensagem(this.mensagem).subscribe(
        sucesso => {
          this.mensagem = new Mensagem();
          form.resetForm();
          this.alertService.success('Mensagem enviada com sucesso');
        },
        erro => {
          this.alertService.error('Erro ao enviar mensagem');
        },
        () => {

        }
      );
    }
  }
}
