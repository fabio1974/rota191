import { Component, OnInit, OnDestroy, Inject, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ResolveStart, ActivatedRoute } from '@angular/router';

import { navItems } from 'src/app/core/_nav';
import { AuthService } from '../_services/auth.service';
import { DOCUMENT } from '@angular/common';
import { DialogService } from 'src/app/shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnDestroy {

  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  constructor(
    public segurancaService: SegurancaService,
    private auth: AuthService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    public dialogService: DialogService,
    @Inject(DOCUMENT) _document?: any
  ) {
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  logout(): void {
    this.auth.salvarLogout();
    this.auth.logout();
    this.router.navigateByUrl('login');
  }

  get nomeUsuario(): string {
    return this.auth.autenticado ? this.auth.nomeUsuario : undefined;
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }
}
