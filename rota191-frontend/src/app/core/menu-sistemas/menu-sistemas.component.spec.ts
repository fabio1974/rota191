import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSistemasComponent } from './menu-sistemas.component';

describe('MenuSistemasComponent', () => {
  let component: MenuSistemasComponent;
  let fixture: ComponentFixture<MenuSistemasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSistemasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSistemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
