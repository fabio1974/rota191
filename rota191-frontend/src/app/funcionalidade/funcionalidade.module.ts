import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionalidadeRouting } from './funcionalidade.routing';
import { Exemplo1Component } from './exemplo1/exemplo1.component';
import { Exemplo2Component } from './exemplo2/exemplo2.component';
import {BsDatepickerModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FuncionalidadeRouting,
    BsDatepickerModule.forRoot(),
  ],
  declarations: [Exemplo1Component, Exemplo2Component]
})
export class FuncionalidadeModule { }
