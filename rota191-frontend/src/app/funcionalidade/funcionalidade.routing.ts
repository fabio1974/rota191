import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Exemplo1Component } from './exemplo1/exemplo1.component';
import { Exemplo2Component } from './exemplo2/exemplo2.component';
import {AuthGuard} from '../seguranca/auth.guard';


const routes: Routes = [
    {
        path: 'exemplo1',
        component: Exemplo1Component,
        canActivate: [AuthGuard],
        data: {
            title: 'Funcionalidade 1'
        }
    },
    {
        path: 'exemplo2',
        component: Exemplo2Component,
        canActivate: [AuthGuard],
        data: {
            title: 'Funcionalidade 2'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class FuncionalidadeRouting { }
