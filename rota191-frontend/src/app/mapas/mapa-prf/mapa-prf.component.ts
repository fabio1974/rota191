import { ToastService } from './../../core/_services/toast.service';
import {Component, Input, OnInit} from '@angular/core';
import {MapasService} from '../mapas.service';

import {getMapaObject, MapaObject} from '../../shared/mapaUtils';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {SearchService} from '../../shared/search.service';
import {Pessoa} from '../../shared/_model/model';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {ptBrLocale} from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap';


@Component({
  selector: 'app-mapa-prf',
  templateUrl: './mapa-prf.component.html',
  styleUrls: ['./mapa-prf.component.css']
})
export class MapaPrfComponent implements OnInit {

  smartphoneIcon = {
    url: '../../../assets/img/mobile-alt.svg',
    scaledSize: {
      width: 30,
      height: 49
    }
  }

  ufs = [
    { label: 'TODAS', value: 'A' }, { label: 'Acre', value: 'AC' }, { label: 'Alagoas', value: 'AL' },
    { label: 'Amapá', value: 'AP' }, { label: 'Amazonas', value: 'AM' }, { label: 'Bahia', value: 'BA' },
    { label: 'Ceará', value: 'CE' }, { label: 'Distrito Federal', value: 'DF' }, { label: 'Espírito Santo', value: 'ES' },
    { label: 'Goiás', value: 'GO' }, { label: 'Maranhão', value: 'MA' }, { label: 'Mato Grosso', value: 'MT' },
    { label: 'Mato Grosso do Sul', value: 'MS' }, { label: 'Minas Gerais', value: 'MG' }, { label: 'Pará', value: 'PA' },
    { label: 'Paraíba', value: 'PB' }, { label: 'Paraná', value: 'PR' }, { label: 'Pernambuco', value: 'PE' },
    { label: 'Piauí', value: 'PI' }, { label: 'Rio de Janeiro', value: 'RJ' }, { label: 'Rio Grande do Norte', value: 'RN' },
    { label: 'Rio Grande do Sul', value: 'RS' }, { label: 'Rondônia', value: 'RO' }, { label: 'Roraima', value: 'RR' },
    { label: 'Santa Catarina', value: 'SC' }, { label: 'São Paulo', value: 'SP' }, { label: 'Sergipe', value: 'SE' },
    { label: 'Tocantins', value: 'TO' },
  ]
  imei
  inicio: Date = null
  horaInicio: Date = null
  horaInicioString: string
  fim: Date = null
  horaFim: Date = null
  horaFimString: string
  pessoas: Pessoa[] = []
  mapaObject: MapaObject = getMapaObject([])
  mapTypeId = 'roadmap'
  uf = 'PI'
  checked: true

  constructor(private mapasService: MapasService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService,
              private searchService: SearchService,
              private bootstrapLocaleService: BsLocaleService,
              private toastService: ToastService
  ) {
    defineLocale('pt-br', ptBrLocale)
    this.bootstrapLocaleService.use('pt-br')
    this.uf = segurancaService.decodedToken.ufLotacao
  }

  ngOnInit() {
    this.inicio = new Date()
    this.fim = new Date()
    this.horaFim = new Date()
    this.horaInicio = new Date()

    this.horaInicio.setHours(0, 0, 0, 0)
    this.horaFim.setHours(23, 59, 59, 999)

    this.horaInicioString = '00:00'
    this.horaFimString = '23:59'

    this.mostrarPessoas()
  }

  private findAllByDates() {
    this.dialogService.showWaitDialog('Aguarde...')
    this.mapasService.findAllByDates(this.uf, this.inicio, this.fim).subscribe(
      resp => {
        this.dialogService.closeWaitDialog()
        this.pessoas = resp as Pessoa[];
        this.mapaObject = getMapaObject(this.pessoas)

        console.log('Mapa Object',this.mapaObject)

        if(this.pessoas.length <=0)
          this.dialogService.showDialog('Atenção', 'Sem rastreamento no período selecionado!')
        //else
        //this.mapaObject = getMapaObject(this.pessoas)
      },
      error => {
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog('Erro', error)
      });
  }

  changeMap($event: any) {
    if(this.mapTypeId == 'roadmap')
      this.mapTypeId = 'hybrid'
    else
      this.mapTypeId = 'roadmap'
  }

  changeUf() {

  }

  changeHoraInicio() {
    this.horaInicioString = ''
    if (this.horaInicio === null) return
    this.horaInicioString += this.horaInicio.getHours().toString().padStart(2, '00')
    this.horaInicioString += ':'
    this.horaInicioString += this.horaInicio.getMinutes().toString().padStart(2, '00')
  }

  changeHoraFim() {
    this.horaFimString = ''
    if (this.horaFim === null) return
    this.horaFimString += this.horaFim.getHours().toString().padStart(2, '00')
    this.horaFimString += ':'
    this.horaFimString += this.horaFim.getMinutes().toString().padStart(2, '00')
  }

  mostrarPessoas() {
    if (!this.inicio || !this.horaInicio || !this.fim || !this.horaFim) {
      this.toastService.toastError('Erro', 'Campos de data inicial ou final inválidos')
      return
    }

    this.inicio.setHours(this.horaInicio.getHours(), this.horaInicio.getMinutes(), 0, 0)
    this.fim.setHours(this.horaFim.getHours(), this.horaFim.getMinutes(), 0, 0)

    if (this.fim.getTime() < this.inicio.getTime()) {
      this.toastService.toastError('Erro', 'Data final não pode ser antes da data inicial')
      return
    }

    if (this.fim.getTime() - this.inicio.getTime() > (24 * 60 * 60 * 1000)) { // 24 horas em milisegundos
      this.toastService.toastError('Erro', 'Intervalo máximo de 24 horas')
      return
    }

    this.findAllByDates()
  }
}
