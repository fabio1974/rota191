import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import {MapasService} from './mapas.service';
import {DialogService} from '../shared/dialog.service';
import {AutoCompleteModule, CalendarModule, DropdownModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {MapaPrfComponent} from './mapa-prf/mapa-prf.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { PopoverModule } from 'ngx-bootstrap/popover';

@NgModule({
  declarations: [MapaPrfComponent],
  imports: [
    AutoCompleteModule,
    CommonModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCg5eE_buXJLsJZbnTZ7z3MnJBOV3_RoYc'
    }),
    FormsModule,
    DropdownModule,
    CalendarModule,
    TimepickerModule,
    PopoverModule
  ],
  providers:[
    MapasService,DialogService
  ]
})
export class MapasModule { }
