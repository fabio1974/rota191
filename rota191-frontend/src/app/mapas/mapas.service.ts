import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {Pessoa} from '../shared/_model/model';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MapasService {

  constructor(private http: HttpClient, private router: Router) { }

/*
  findAllByDates(uf: string, inicio: Date, fim: Date): Observable<Pessoa[]> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')

    let params = new HttpParams()
    if (uf && uf !== 'A') { params = params.append('uf', uf) ; }
    params = params.append('dataInicio', inicio.toString())
    params = params.append('dataFim', fim.toString())

    // console.log(`${environment.apiUrl}/findAllByDates?uf=string&dataInicio=string&dataFim=string`)

    return this.http.get<Pessoa[]>(`${environment.apiUrl}/findAllByDates`, { headers, params });
  }
*/


  findAllByDates(uf: string, inicio: Date, fim: Date): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(`${environment.apiUrl}/findTeste/?uf=${uf}&dataInicio=${inicio.toISOString()}&dataFim=${fim.toISOString()}`);
  }
}
