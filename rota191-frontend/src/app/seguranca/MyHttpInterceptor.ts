import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';



@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

  constructor() {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (req.body) {
      const reqAfter = req.clone({body:JSON.parse(JSON.stringify(req.body))});
      return next.handle(reqAfter).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log(event)
          event = event.clone({body: this.convertToDate(event.body)});
        }
        return event;
      }));
    }
    return next.handle(req);
  }



  convertToDate(body) {
    if (body === null || body === undefined) {
      return body;
    }
    if (typeof body !== 'object') {
      return body;
    }
    for (const key of Object.keys(body)) {
      const value = body[key];
      if (this.isIso8601(value)) {
        body[key] = new Date(value);
      } else if (typeof value === 'object') {
        this.convertToDate(value);
      }
    }
  }

  isIso8601(value) {
    if (value === null || value === undefined) {
      return false;
    }
    return this.iso8601.test(value);
  }

  iso8601 = /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/;

}
