import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {SegurancaService} from './seguranca.service';
import {tryCatch} from 'rxjs/internal-compatibility';
import {savePriviousRoute} from './saveHistoryNavigation';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private segurancaService: SegurancaService,
              private router: Router){
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)   :Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(this.router.navigated){
      console.log("this.router.navigated",this.router.navigated)
      savePriviousRoute(state)

    }
    if(!this.segurancaService.isLogado() || this.segurancaService.isTokenExpired()){
      console.log("this.segurancaService.isLogado()",this.segurancaService.isLogado())
      this.router.navigate(['/login']);
      return false
    }
    return true;
  }

}
