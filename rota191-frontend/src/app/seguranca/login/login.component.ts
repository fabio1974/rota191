import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {SegurancaService} from '../seguranca.service';
import { JwtHelperService } from '@auth0/angular-jwt';


export class LoginPayload{
  cpf=null;
  senha=null;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  error:boolean = false

  constructor(private router: Router,public segurancaService: SegurancaService) {
  }

  cpf
  senha

  login(form: NgForm): void {
    if (form.valid) {



      var payload = new LoginPayload();
      payload.cpf = form.value.cpf;
      payload.senha = form.value.senha;

      this.segurancaService.login(payload).subscribe(
        resp => {

          this.segurancaService.decodedToken = new JwtHelperService().decodeToken(resp.token);
          console.log("decodedToken",this.segurancaService.decodedToken)
          localStorage.setItem('token', resp.token);
          this.router.navigate(['/mapaPrf']);
        },
        error => {
          this.error = true;
          console.log('error', error);
        });
    }
  }

}
