import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {SegurancaService} from './seguranca.service';
import {FormsModule} from '@angular/forms';
import {JwtModule} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {AppFooterModule, AppHeaderModule} from '@coreui/angular';
import {CoreModule} from '../core/core.module';
import {SharedModule} from '../shared/shared.module';


export function tokenGetter(){
  var token = localStorage.getItem('token');
  return  token;
}


@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: environment.whitelistedDomains,
        blacklistedRoutes: environment.blacklistedRoutes,
        skipWhenExpired: true
      }
    }),
    AppHeaderModule,
    CoreModule,
    SharedModule,
    AppFooterModule
  ],
  providers:[
    SegurancaService
  ]
})
export class SegurancaModule { }
