import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginPayload} from './login/login.component';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {Pessoa} from '../shared/_model/model';



@Injectable({
  providedIn: 'root'
})
export class SegurancaService {

  decodedToken: any;

  constructor(private http: HttpClient, private router: Router) {
     var token = localStorage.getItem("token")
    if(token) {
      this.decodedToken = new JwtHelperService().decodeToken(token);
      console.log("decodedToken",this.decodedToken)
    }
  }

  login(loginPayload:LoginPayload):Observable<any>{
    return this.http.post<LoginPayload>(`${environment.apiUrl}/login`,loginPayload)
  }

  isTokenExpired(){
    var token = localStorage.getItem("token")
    return new JwtHelperService().isTokenExpired(token)
  }

  logout(){
    localStorage.removeItem("token")
    this.decodedToken=null
    window.location.replace('/login')
    //this.router.navigate(['/login']);
  }

  temPermissao(permissao:string){
    return this.decodedToken && this.decodedToken.roles.includes(permissao)
  }

  /*pessoaLogadaId(){
    return this.decodedToken.id
  }*/

  pessoaLogada(){
    let pessoa = new Pessoa()
    return pessoa
  }



  isLogado() {
    return this.decodedToken && this.decodedToken.cpf
  }
}
