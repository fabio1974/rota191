import { Component, OnInit } from '@angular/core';
import { Alert } from '../_model/alert';
import { AlertService } from '../../core/_services/alert.service';
import { AuthService } from 'src/app/core/_services/auth.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  alerts: Alert[] = [];

  constructor(
    private alertService: AlertService,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.alertService.getAlert().subscribe((alert: Alert) => {
      if (!alert) {
        // clear alerts when an empty alert is received
        this.alerts = [];
        return;
      }

      // add alert to array
      this.alerts.push(alert);
    });
  }

  removeAlert(alert: Alert) {
    this.auth.cancelaFalhaLogin();
    this.auth.cancelaLogout();
    this.alerts = this.alerts.filter(x => x !== alert);
  }

}
