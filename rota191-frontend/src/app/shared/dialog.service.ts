import {ChangeDetectorRef, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor( ) {  }

  public messageTitle
//  private messageTitleWait
  public messageText
  public displayDialog=false
  public displayWaitDialog=false

  showDialog(title, text){
    Promise.resolve(null).then(() => {
      this.displayDialog = true
      this.messageText = text
      this.messageTitle= title
    })
  }

  showWaitDialog(title){
    Promise.resolve(null).then(() =>{
        this.displayWaitDialog = true
        this.messageTitle= title
    })
  }

  closeWaitDialog(){
    this.displayWaitDialog = false
  }






}
