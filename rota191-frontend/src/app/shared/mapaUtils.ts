import {Pessoa} from './_model/model';

export class MapaObject{
  lastLocations: Pessoa[];
  centro:Pessoa;
  zoom:number;
  pNE:Pessoa
  pSW:Pessoa
}

export function getMapaObject(amostra:Pessoa[]){
  let mapa = new MapaObject()
  let centro = new Pessoa();
  mapa.centro = centro ;

  if(amostra.length==0){
    mapa.centro.locLongitude =-51.82
    mapa.centro.locLatitude =-13.34
    mapa.pNE = mapa.centro
    mapa.pSW = mapa.centro
    mapa.zoom = 4
  }else if(amostra.length==1){
    mapa.centro.locLatitude = amostra[0].locLatitude
    mapa.centro.locLongitude = amostra[0].locLongitude
    mapa.pNE = mapa.centro
    mapa.pSW = mapa.centro
    mapa.zoom = 21
  }else {

    let maxLon= -181;
    let maxLat= -91;
    let minLon= 181;
    let minLat= 91;

    amostra.forEach(loc=>{
      if(maxLon < loc.locLongitude)
        maxLon = loc.locLongitude;
      if(maxLat < loc.locLatitude)
        maxLat = loc.locLatitude
      if(minLon > loc.locLongitude)
        minLon = loc.locLongitude
      if(minLat > loc.locLatitude)
        minLat = loc.locLatitude;
    })

    let ne = new Pessoa();
    ne.locLatitude =maxLat;
    ne.locLongitude =maxLon;
    mapa.pNE =ne;

    let sw = new Pessoa();
    sw.locLatitude =minLat;
    sw.locLongitude =minLon;
    mapa.pSW =sw;

    mapa.centro.locLatitude = (minLat+maxLat)/2
    mapa.centro.locLongitude= (minLon+maxLon)/2;

    mapa.zoom = getBestZoom(mapa.pNE, mapa.pSW);
  }
  return mapa;
}


export function getBestZoom(pNE:Pessoa, pSW:Pessoa){
  const GLOBE_WIDTH = 256;
  let angleX = pNE.locLongitude - pSW.locLongitude;
  let angleY = pNE.locLatitude - pSW.locLatitude;
  let delta = 0;
  if(angleY > angleX) {
    angleX = angleY;
    delta = 1;
  }
  if (angleX < 0) {
    angleX += 360;
  }
  let N = Math.log(960 * 360 / angleX / GLOBE_WIDTH);
  let r = Math.floor(N/Math.log(2));
  let zoom = r as number - delta;
  if(zoom>21)
    zoom = 21;
  return  zoom;
}
