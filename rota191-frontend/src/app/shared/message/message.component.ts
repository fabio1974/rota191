import { Component, OnInit, Input } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input()
  control: FormControl;
  @Input()
  form: NgForm;
  @Input()
  extraCondicao;

  constructor() { }

  ngOnInit() {
  }

}
