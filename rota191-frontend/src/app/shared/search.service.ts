import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }



 /* searchClientes(nome){
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchClientes?${query}`);
  }*/

/*  searchMotoristas(clienteId){
    const query = `clienteId=${clienteId}`;
    return this.http.get(`${environment.apiUrl}/searchMotoristas?${query}`);
  }*/

 /* searchViagens(cliente,numeroViagem){
    const query = `clienteId=${cliente.id}&numeroViagem=${numeroViagem}`;
    return this.http.get(`${environment.apiUrl}/searchViagens?${query}`);
  }*/


  searchMunicipios(nome){
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchMunicipios?${query}`);
  }


/*
  searchContratos(nome) {
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchContratos?${query}`);
  }
*/

  searchEquipamentos(imei) {
    const query = `imei=${imei}`;
    return this.http.get(`${environment.apiUrl}/searchEquipamentos?${query}`);
  }

 /* searchEmpregados(nome) {
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchEmpregados?${query}`);
  }*/

/*
  searchChips(iccid) {
    const query = `iccid=${iccid}`;
    return this.http.get(`${environment.apiUrl}/searchChips?${query}`);
  }
*/

  /*empregadosResult
  searchEmpregados(event) {
    const query = `nome=${event.query}`;
    this.http.get(`${environment.apiUrl}/searchEmpregados?${query}`).subscribe(
      resp=>{
        this.empregadosResult = resp as Empregado[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }
*/




}
