import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageComponent } from './message/message.component';
import { AlertComponent } from './alert/alert.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import {FormsModule} from '@angular/forms';
import {BsDatepickerModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    AlertModule,
    FormsModule

  ],
  declarations: [MessageComponent, AlertComponent],
  exports: [MessageComponent, AlertComponent, BsDatepickerModule]
})
export class SharedModule { }
