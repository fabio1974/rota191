export const environment = {
  production: true,
  VERSAO: '1.0.1',
  apiUrl: '/api' ,
  whitelistedDomains: [new RegExp('/api')],
  blacklistedRoutes: [new RegExp('/login')],
};
